const https = require('https')

const start = Date.now()

const doRequest = () => {
  let i = 0
  const results = {}
  return () => {
    https
      .request('https://www.google.com', (res) => {
        res.on('data', () => {})
        res.on('end', () => {
          i++
          results[i] = Date.now() - start
          console.log(`${i}: `, Date.now() - start)
          console.log(results)
        })
      })
      .end()
  }
}

const requestFn = doRequest()
requestFn()
requestFn()
requestFn()
requestFn()
requestFn()
requestFn()